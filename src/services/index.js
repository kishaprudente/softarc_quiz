import setupUsersService from './users';
import setupItemsService from './items';
import setupQuizzesService from './quizzes';

const setupAllServices = (db) => {
  return function () {
      const app = this;

      app
          .configure(setupUsersService(db))
          .configure(setupItemsService(db))
          .configure(setupQuizzesService(db))
  }
}

export default setupAllServices;
