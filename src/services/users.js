import feathersMongo from 'feathers-mongodb';

export default function setupUsersService(db) {
  return function setup() {
    const app = this;
    const collectionName = 'users';

    app.use(`api/${collectionName}`, feathersMongo({
      Model: db.collection(collectionName),
    }));

    app.service(`api/${collectionName}`)
      .hooks({
        before: {
          get: [],
          find: [],
          create: [],
          patch: [],
          update: [],
          remove: [],
        },
        after: {
          get: [],
          find: [],
          create: [],
          patch: [],
          update: [],
          remove: [],
        }
      })
  }
}