import authentication, { hooks } from '@feathersjs/authentication';
import local from '@feathersjs/authentication-local';
import jwt from '@feathersjs/authentication-jwt';

const setupService = () => {
    return function() {
        const app = this;

        app.configure(authentication({
            service: '/api/users',
            entity: 'user',
            secret: 's3cr3tsEcr3tpa$$',
            path: '/api/authentication',
            local: {
                usernameField: 'username',
            },
        }))
        .configure(local())
        .configure(jwt())
        .service('/api/authentication').hooks({
            before: {
                create: [hooks.authenticate(['jwt', 'local'])],
                find: [hooks.authenticate('jwt')],
            },
            after: {
    
            }
        });
    }
};

export default setupService;
