import feathersMongo from 'feathers-mongodb';
import hooks from 'feathers-authentication-hooks';

export default function setupItemsService(db) {
  return function setup() {
    const app = this;
    const collectionName = 'items';

    app.use(`/api/${collectionName}`, feathersMongo({
      Model: db.collection(collectionName),
    }));

    app.service(`/api/${collectionName}`)
      .hooks({
        before: {
          get: [],
          find: [],
          create: [],
          patch: [],
          update: [],
          remove: [],
          all: [
            hooks.restrictToOwner({ownerField: 'role'})
          ]
        },
        after: {
          get: [],
          find: [],
          create: [],
          patch: [],
          update: [],
          remove: [],
        }
      })
  }
}