import feathersMongo from 'feathers-mongodb';
import {
  fastJoin,
  iff,
  isProvider
} from 'feathers-hooks-common';

import Quiz from '../models/Quiz';

export default function setupUsersService(db) {
  return function setup() {
    const app = this;
    const collectionName = 'quizzes';

    app.use(`/api/${collectionName}`, feathersMongo({
      Model: db.collection(collectionName),
    }));

    const quizResolvers = {
      joins: {
        items: () => async (quiz) => {
          quiz.items = (await app.service('api/items').find({
            query: { _id: { $in: quiz.itemIds } },
          }));
        },
        taker: () => async (quiz) => {
          quiz.taker = (await app.service('api/users').find({
            query: { _id: { $in: quiz.userId } },
          }))
        } 
      }
    }

    app.service(`/api/${collectionName}`)
      .hooks({
        before: {
          get: [],
          find: [],
          create: [
            iff(
              isProvider('external')
            )
          ],
          patch: [
            iff(
              isProvider('external')
            )
          ],
          update: [
            iff(
              isProvider('external')
            )
          ],
          remove: [
            iff(
              isProvider('external')
            )
          ],
        },
        after: {
          get: [],
          find: [],
          create: [],
          patch: [],
          update: [],
          remove: [],
          all: [
            fastJoin(quizResolvers)
          ]
        }
      })
  }
}