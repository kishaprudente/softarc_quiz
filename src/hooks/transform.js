function transformHook(Class) {
  return function hook(hookDetails) {
    const hookCopy = hookDetails;
    const info = hookDetails.type === 'before' ? 'data' : 'result';

    if (hookDetails[info] instanceof Array) {
      const transformData = hookDetails[info].map(data => new Class(data));
      hookCopy[info] = transformData;
    } else if (hookDetails[info] instanceof Object) {
      hookCopy[info] = new Class(hookDetails[info]);
    } else {
      return new Error('Transform Hook used wrong');
    }

    return hookDetails;
  };
}

export default transformHook;
