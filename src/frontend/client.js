import feathers from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import authenticationClient from '@feathersjs/authentication-client';
import io from 'socket.io-client';

const url = `${window.location.protocol}//${window.location.host}`;
const socket = io(url);

const client = feathers()
  .configure(socketio(socket))
  .configure(authenticationClient({ storage: window.localStorage }));

client.service.bind(client);

window.client = client;

client.on('connection', connection => {
  client.channel('anonymous').join(connection);
});

export default client;
