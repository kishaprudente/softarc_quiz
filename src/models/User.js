import Joi from 'joi';
import { decorate, observable } from 'mobx';
import Model from './Model';

class User extends Model {
  constructor(props) {
    const defaults = {
      firstName: '',
      lastName: '',
    };
    super({ ...defaults, ...props });
  }

  static get schema() {
    return {
      firstName: Joi.string().max(30).required(),
      lastName: Joi.string().max(30).required(),
    };
  }
}

decorate(User, {
  firstName: observable,
  lastName: observable,
});

export default User;
