import Joi from 'joi';
import { decorate, observable, action } from 'mobx';
import Model from './Model';
import User from './User';

class Quiz extends Model {
  constructor(props) {
    const defaults = {
      quizzes: [],
      items: [],
      taker: '',
      itemIds: [],
      userId: ''
    };
    super({ ...defaults, ...props });
  }
  
  addItem = (user) => {
    if (user.role != 'Teacher') {
      throw new Error('Only teachers can make the quiz items.');
    } else {
      this.items.push(item);
      this.items.push(item._id);
    }
  }
  
  removeItem = (user, _id) => {
    if (user.role != 'Teacher') {
      throw new Error('Cannot remove item.')
    } else {
      this.item = this.items.filter(item => item._id !== _id);
      this.itemIds = this.itemIds.filter(id => id !== _id);
    }
  }
}

decorate(Quiz, {
  items: observable,
  taker: observable,
  addItem: action,
  removeItem: action,
});

export default User;
