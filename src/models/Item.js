import Joi from 'joi';
import { decorate, observable } from 'mobx';
import Model from './Model';

class Item extends Model {
  constructor(props) {
    const defaults = {
      question: '',
      choices: [],
      correctAnswer: '',
    };
    super({ ...defaults, ...props });
  }

  static get schema() {
    return {
      question: Joi.string().required(),
      choices: Joi.string().required(),
      correctAnswer: Joi.string().required(),
    };
  }
}

decorate(Item, {
  question: observable,
  choices: observable,
  correctAnswer: observable,
});

export default Item;
