/* eslint no-return-assign: "error" */
import {
  action,
  decorate,
} from 'mobx';

class Model {
  constructor(props) {
    Object.assign(this, props);
  }

  static get schema() {
    return {};
  }

  get errors() {
    const { error } = this.validate();

    if (error) {
      const { details } = error;
      return details;
    }

    return [];
  }

  get isValid() {
    const { error } = this.validate();
    return error === null;
  }
}

decorate(Model, {
  setProperty: action,
});

export default Model;
