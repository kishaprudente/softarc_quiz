import feathers from '@feathersjs/feathers';
import express from '@feathersjs/express';
import socketio from '@feathersjs/socketio';
import { MongoClient } from 'mongodb';
import { join } from 'path';
import setupServices from '../services/';

const app = express(feathers());

app
  .configure(socketio())
  .configure(express.rest())
  .use(express.json())
  .use(express.urlencoded({ extended: true }))
  .use(express.static(join(process.cwd(), 'public')));

const initServer = async () => {
  const client = await MongoClient.connect('mongodb://localhost:27017/quiz');
  const db = client.db(); 
  app.configure(setupServices(db));

  return app;
}

const initApp = async () => {
  const init = await initServer();
  try {
    init.listen(process.env.PORT || 5000, () => {
      console.log('App Live!');
    });
  } catch (e) {
    console.error(e);
  }
};

initApp();
