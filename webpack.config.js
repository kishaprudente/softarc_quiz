const path = require('path');
// const webpack = require(webpack);

const publicPath = path.resolve(`${process.cwd()}/public/`);
const srcPath = path.resolve(`${process.cwd()}/src/`);

const options = {
  cache: true,

  entry: {
    index: path.join(`${process.cwd()}/src/frontend/index.jsx`),
  },

  output: {
    filename: '[name].[contentHash].js',
    chunkFilename: '[name].[contentHash].js',
    path: path.join(`${publicPath}/js`),
    publicPath: 'js/',
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [{
          loader: 'babel-loader?cacheDirectory',
          options: {
            presets: [
              ['@babel/preset-env', {
                targets: {
                  browsers: ['last 2 versions', 'safari >= 7'],
                  node: 'current',
                },
              }],
              '@babel/preset-react',
            ],
            cacheDirectory: true,
            plugins: [
              ['import', { libraryName: 'antd', libraryDirectory: 'es', style: 'css' }],
              ['@babel/plugin-transform-runtime', {
                corejs: 2,
                regenerator: true,
              }],
            ],
          },
        }],
      }, {
        test: /(?!.*st.css$)(^.*css$)/,
        use: [{
          loader: 'style-loader',
        }, {
          loader: 'css-loader',
        }, {
          loader: 'sass-loader',
        }],
      }, {
        test: /\.(ttf|eot|svg|woff(2)?)$/,
        use: [{
          loader: 'url-loader',
        }],
      },
    ],
  },

  resolve: {
    extensions: ['.js', '.jsx'],
  },

  plugins: [],
};

module.exports = options;
