import { ObjectId } from 'mongodb';

const items = [
  {
    _id: ObjectId('321cba21'),
    question: 'What is 1 + 1?',
    choiceA: '1',
    choiceB: '2',
    choiceC: '3',
    choiceD: 'None of the above',
    correctAnswer: 'B',
  },
  {
    _id: ObjectId('123abc12'),
    question: 'Where is CPU located',
    choiceA: 'Jaro',
    choiceB: 'Mandurriao',
    choiceC: 'Lapaz',
    choiceD: 'None of the above',
    correctAnswer: 'A',
  }
]

export default questions;