import faker from 'faker';

import initServer from '../src/backend/server';

import users from './users';
import questions from './questions';

const init = async () => {
  const app = await initServer();

  console.log('SEEDING...');

  try {
    await app.service('/api/users').remove(null);
    await app.service('/api/users').create(users);

    console.log('SEEDING DONE.');
  } catch (e) {
    console.log(e);
  }

  process.exit(0);
};

init();