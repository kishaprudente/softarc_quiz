import { ObjectId } from 'mongodb';

const users = [
  {
    _id: ObjectId('1ab2c3'),
    firstName: 'Maria',
    lastName: 'Deby',
    role: 'Teacher',
  },
  {
    _id: ObjectId('4d5e6f'),
    firstName: 'Jessica',
    lastName: 'Reyes',
    role: 'Student'
  },
  {
    _id: ObjectId('7gh8i9'),
    firstName: 'Juan',
    lastName: 'Luna',
    role: 'Student'
  }
]

export default users;